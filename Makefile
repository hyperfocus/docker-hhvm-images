REG_ADDRESS=registry.gitlab.com
USERNAME=supernami
VERSION=1.0

hhvm-build:
	docker-compose -f hhvm.yml build

hhvm-tag:
	docker tag hhvm:latest ${REG_ADDRESS}/${USERNAME}/docker-hhvm-images:base-${VERSION} && \
	docker tag ${REG_ADDRESS}/${USERNAME}/docker-hhvm-images:base-${VERSION} ${REG_ADDRESS}/${USERNAME}/docker-hhvm-images:base-latest

hhvm-push:
	docker push ${REG_ADDRESS}/${USERNAME}/docker-hhvm-images:base-${VERSION} && \
	docker push ${REG_ADDRESS}/${USERNAME}/docker-hhvm-images:base-latest

hhvm-build-config:
	docker-compose -f hhvm-config.yml build
